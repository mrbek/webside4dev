﻿import React, { Component } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import "./App.css";
import NavBar from "./components/NavBar";
import Footer from "./components/Footer";

import Home from "./components/Home";
import Other from "./components/Other";
import Stat from "./components/Stat";

//TODO Web Template Studio: Add routes for your new pages here.
class App extends Component {
  render() {
    return (
      <React.Fragment>
        <NavBar />
        <Switch>
        <div>
        <h1>TEst</h1>

   {/* Add information here! */}
      </div>
          <Redirect exact path = "/" to = "/Home" />
          <Route path = "/Home" component = { Home } />
          <Route path = "/Other" component = { Other } />
          <Route path = "/Stat" component = { Stat } />
          
        </Switch>
        <Footer />
      </React.Fragment>
    );
  }
}

export default App;
